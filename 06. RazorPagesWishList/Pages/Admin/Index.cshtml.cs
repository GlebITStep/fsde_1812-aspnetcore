﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WishList.Models;

namespace WishList.Pages.Admin
{
    public class IndexModel : PageModel
    {
        private readonly WishList.Models.WishListDbContext _context;

        public IndexModel(WishList.Models.WishListDbContext context)
        {
            _context = context;
        }

        public IList<Item> Item { get;set; }

        public async Task OnGetAsync()
        {
            Item = await _context.Items.ToListAsync();
        }
    }
}
