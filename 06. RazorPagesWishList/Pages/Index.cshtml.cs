﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using WishList.Models;

namespace WishList.Pages
{
    public class IndexModel : PageModel
    {
        private readonly WishListDbContext context;
        private readonly IHttpClientFactory httpClientFactory;

        public IndexModel(WishListDbContext context, IHttpClientFactory httpClientFactory)
        {
            this.context = context;
            this.httpClientFactory = httpClientFactory;
        }

        //[BindProperty]
        //public Item Item { get; set; }

        [BindProperty]
        public UserInput Input { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            var httpClient = httpClientFactory.CreateClient();
            var response = await httpClient.GetAsync(Input.Url);
            var html = await response.Content.ReadAsStringAsync();

            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);
            var htmlHead = htmlDoc.DocumentNode.SelectSingleNode("//head");
            var metaOgImage = htmlHead
                .Descendants()
                .FirstOrDefault(x => x.Attributes["property"]?.Value == "og:image");

            var imageUrl = metaOgImage.Attributes["content"]?.Value ?? "placeholder.png";

            var item = new Item
            {
                Url = Input.Url,
                Title = Input.Title,
                Price = Input.Price,
                ImageUrl = imageUrl
            };

            context.Items.Add(item);
            await context.SaveChangesAsync();

            return RedirectToPage("/Items");
        }

        public class UserInput
        {
            [Required]
            [Url]
            public string Url { get; set; }

            [Required]
            [MaxLength(100)]
            public string Title { get; set; }

            [Required]
            public decimal Price { get; set; }
        }
    }






    ////[IgnoreAntiforgeryToken]
    //public class IndexModel : PageModel
    //{
    //    [BindProperty(SupportsGet = true)]
    //    [Required]
    //    public string Message { get; set; }

    //    public void OnGet()
    //    {
    //        //Message = "Hello, User!";
    //        //Message = Message.ToLower();
    //    }

    //    public void OnPost()
    //    {
    //        if (ModelState.IsValid)
    //            Message = Message.ToUpper();
    //        else
    //            Message = "Empty!";
    //    }
    //}
}
