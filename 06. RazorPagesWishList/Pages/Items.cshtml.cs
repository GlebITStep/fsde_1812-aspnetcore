﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WishList.Models;

namespace WishList.Pages
{
    public class ItemsModel : PageModel
    {
        private readonly WishListDbContext context;

        public ItemsModel(WishListDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<Item> Items { get; set; }

        public void OnGet()
        {
            Items = context.Items;
        }

        public void OnGetSortByPrice(int type = 0)
        {
            if (type == 0)
                Items = context.Items.OrderBy(x => x.Price);
            else
                Items = context.Items.OrderByDescending(x => x.Price);
        }
    }
}