﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WishList.Models
{
    public class Item
    {
        public int Id { get; set; }

        public string ImageUrl { get; set; }

        [Required]
        public string Url { get; set; }

        [Required]
        [MaxLength(200)]
        public string Title { get; set; }


        public decimal Price { get; set; }
    }
}
