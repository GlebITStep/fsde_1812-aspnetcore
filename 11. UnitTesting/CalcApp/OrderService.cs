﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CalcApp
{
    public class OrderService
    {
        private readonly IProductRepository productRepository;

        public OrderService(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public decimal PriceWithDiscount(decimal price, DateTime currentDate)
        {
            if (currentDate.DayOfWeek == DayOfWeek.Sunday)
                return price * 0.8M;
            return price;
        }

        public decimal CartTotal()
        {
            return productRepository.GetAll().Sum(x => x.Price); 
        }
    }
}
