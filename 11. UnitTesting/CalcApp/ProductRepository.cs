﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalcApp
{
    public class Product
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
    }

    public interface IProductRepository
    {
        IEnumerable<Product> GetAll();
    }

    public class ProductRepository : IProductRepository
    {
        public IEnumerable<Product> GetAll()
        {
            return null;
        }
    }
}
