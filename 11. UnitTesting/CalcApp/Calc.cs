﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalcApp
{
    public class Calc
    {
        public int Add(int x, int y)
        {
            checked
            { 
                return x + y; 
            }
        }

        public int Division(int x, int y)
        {
            if (y == 0)
                throw new ArgumentException("Second param can't be zero", nameof(y));

            return x / y;
        }
    }
}
