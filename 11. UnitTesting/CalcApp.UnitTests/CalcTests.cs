﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CalcApp.UnitTests
{
    //TDD - Test Driven Development

    public class CalcTests
    {



        [Theory]
        [InlineData(4, 2, 2)]
        [InlineData(8, 2, 4)]
        public void Division_TwoNumbers_CorrectAnswer(int x, int y, int correctResult)
        {
            var calc = new Calc();

            int result = calc.Division(x, y);

            Assert.Equal(result, correctResult);
        }

        [Fact]
        public void Division_AnyNumberToZero_ThrowsArgumentException()
        {
            var calc = new Calc();

            Assert.Throws<ArgumentException>(() => calc.Division(1, 0));
        }


        [Theory]
        [InlineData(2, 3, 5)]
        [InlineData(0, 1, 1)]
        [InlineData(0, 0, 0)]
        public void Add_TwoNumbers_CorrectAnswer(int x, int y, int correctResult)
        {
            var calc = new Calc();

            var result = calc.Add(x, y);

            Assert.Equal(result, correctResult);
        }

        [Fact]
        public void Add_TwoBigNumbers_ThrowsOverflowException()
        {
            var calc = new Calc();
            
            Assert.Throws<OverflowException>(() => calc.Add(2000000000, 2000000000));
        }


        ////Method_Params/Action_Result
        //[Fact]
        //public void Add_2And3_Returns5()
        //{
        //    //AAA
        //    //Assign
        //    var calc = new Calc();

        //    //Act
        //    var result = calc.Add(2, 3);

        //    //Assert
        //    Assert.Equal("5", result.ToString());
        //}

        //[Fact]
        //public void Add_5And6_Returns11()
        //{
        //    //AAA
        //    //Assign
        //    var calc = new Calc();

        //    //Act
        //    var result = calc.Add(5, 6);

        //    //Assert
        //    Assert.Equal(11, result);
        //}

        //[Fact]
        //public void Add_2000000000And2000000000_Returns4000000000()
        //{
        //    //AAA
        //    //Assign
        //    var calc = new Calc();

        //    //Act
        //    var result = calc.Add(2000000000, 2000000000);

        //    //Assert
        //    Assert.Equal("4000000000", result.ToString());
        //}
    }
}
