﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CalcApp.UnitTests
{
    public class OrderServiceTests
    {
        [Fact]
        public void CartTotal_With2Products_Returns5()
        {
            var fakeProducts = new List<Product>
            {
                new Product { Id = 1, Title = "Bread", Price = 2 },
                new Product { Id = 2, Title = "Tea", Price = 3 },
            };
            var fakeProductRepository = new Mock<IProductRepository>();
            fakeProductRepository.Setup(x => x.GetAll()).Returns(fakeProducts);
            var orderService = new OrderService(fakeProductRepository.Object);

            var result = orderService.CartTotal();

            Assert.Equal(5M, result);
        }

        [Fact]
        public void PriceWithDiscount_Price100AtMonday_Returns100()
        {
            var orderService = new OrderService(null);

            var date = new DateTime(2019, 12, 9);
            var result = orderService.PriceWithDiscount(100, date);

            Assert.Equal(100M, result);
        }

        [Fact]
        public void PriceWithDiscount_Price100AtSunday_Returns80()
        {
            var orderService = new OrderService(null);

            var date = new DateTime(2019, 12, 8);
            var result = orderService.PriceWithDiscount(100, date);

            Assert.Equal(80M, result);
        }
    }
}
