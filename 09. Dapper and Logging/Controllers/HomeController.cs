﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AspDapper.Models;
using AspDapper.Services;

namespace AspDapper.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> logger;
        private readonly UnitOfWork unitOfWork;

        public HomeController(ILogger<HomeController> logger, UnitOfWork unitOfWork)
        {
            this.logger = logger;
            this.unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
            logger.LogTrace("Index page");          //0
            logger.LogDebug("Index page");          //1
            logger.LogInformation("Index page");    //2
            logger.LogWarning("Index page");        //3
            logger.LogError("Index page");          //4
            logger.LogCritical("Index page");       //5
            return View();
        }

        public IActionResult Categories()
        {
            var categories = unitOfWork.Categories.GetAll();
            return View(categories);
        }

        public IActionResult Products()
        {
            var products = unitOfWork.Products.GetAll();
            return View(products);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
