﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspDapper.Services
{
    public class UnitOfWork : IDisposable
    {
        private readonly SqlConnection connection;

        private readonly Lazy<ProductRepository> productRepository;
        private readonly Lazy<CategoryRepository> categoryRepository;

        public UnitOfWork(string connectionString)
        {
            connection = new SqlConnection(connectionString);
            productRepository = new Lazy<ProductRepository>(() => new ProductRepository(connection));
            categoryRepository = new Lazy<CategoryRepository>(() => new CategoryRepository(connection));
        }

        public CategoryRepository Categories => categoryRepository.Value;
        public ProductRepository Products => productRepository.Value;

        public void Dispose()
        {
            connection.Dispose();
            GC.SuppressFinalize(this);
        }

        ~UnitOfWork()
        {
            connection.Dispose();
        }
    }
}
