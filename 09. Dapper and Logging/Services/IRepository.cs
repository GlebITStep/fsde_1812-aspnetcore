﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspDapper.Services
{
    public interface IRepository<T>
    {
        T GetById(int id);
        IEnumerable<T> GetAll();

        //Create

        //Update
        
        //Delete
    }
}
