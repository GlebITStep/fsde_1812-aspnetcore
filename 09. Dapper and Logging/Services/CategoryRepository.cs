﻿using AspDapper.Models;
using Dapper;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspDapper.Services
{
    public class CategoryRepository : IRepository<Category>
    {
        private readonly SqlConnection connection;

        public CategoryRepository(SqlConnection connection)
        {
            this.connection = connection;
        }

        public IEnumerable<Category> GetAll()
        {
            var query = "select * from Categories";
            return connection.Query<Category>(query);
        }

        public Category GetById(int id)
        {
            var query = "select * from Categories where id = @id";
            return connection.QueryFirst<Category>(query, new { id });
        }
    }
}
