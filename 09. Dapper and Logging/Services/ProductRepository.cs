﻿using AspDapper.Models;
using Dapper;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspDapper.Services
{
    public class ProductRepository : IRepository<Product>
    {
        private readonly SqlConnection connection;

        public ProductRepository(SqlConnection connection)
        {
            this.connection = connection;
        }

        public IEnumerable<Product> GetAll()
        {
            var query = "select * from Products";
            return connection.Query<Product>(query);
        }

        public Product GetById(int id)
        {
            var query = "select * from Products where id = @id";
            return connection.QueryFirst<Product>(query, new { id });
        }
    }
}
